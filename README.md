# Commerce Helcim payment gateway

### Helcim `Hosted Pages` integration notes:

* Helcim accepts only CAD currency.
* There are two necessary fields during a new payment method adding at
  `admin/commerce/config/payment-gateways`: Payment Page URL with token and
  secret hash` (can be found in _"Field Settings"_ of Payment Pages
  administration section).
* Helcim allows only static Drupal URLs for success and cancel redirects during
  transactions. It will be:
  - `https://<your-domain>/commerce-helcim/cancel`
  - `https://<your-domain>/commerce-helcim/success`
* `Test mode` seems currently not working on Helcim's side.
  Test card numbers don't go through.
* If Hosted Page fields are marked and "read-only" on the admin page, they won't
  accept values from Drupal's payment module.
* Helcim plans to roll out API updates (according to their support).
